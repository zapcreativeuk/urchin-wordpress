const appHeight = () => {
  const doc = document.documentElement
  doc.style.setProperty("--app-height", `${window.innerHeight}px`)
}
window.addEventListener("resize", appHeight)
appHeight()

AOS.init()

// location.reload(true)

const aosAnimation = document.querySelectorAll(".aos")
observer = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.intersectionRatio > 0) {
      entry.target.classList.add("aos-animate")
    } else {
      entry.target.classList.remove("aos-animate")
    }
  })
})
aosAnimation.forEach(aosObject => {
  observer.observe(aosObject)
})

const hamburgerBars = document.querySelectorAll(".hamburger-menu-line")
const urchinFooter = document.querySelector(".urchin-signoff")
const footerTop = urchinFooter.getBoundingClientRect().top
const urchinProduct = document.querySelector(".product")
const urchinLogoOne = document.querySelector(".urchin-logo-1")
const urchinLogoTwo = document.querySelector(".urchin-logo-2")
const buyCircleTwo = document.querySelector(".buy-circle-2")
const burgerLineOne = document.querySelector(".hamburger-menu-line-1")
const burgerLineTwo = document.querySelector(".hamburger-menu-line-2")
const burgerLineThree = document.querySelector(".hamburger-menu-line-3")

urchinWrapper.addEventListener("scroll", () => {
  const productFooter = urchinProduct.getBoundingClientRect().bottom

  if (productFooter < (window.innerHeight || document.documentElement.clientHeight)) {
    urchinLogoOne.style.fill = "#ffffff"
    urchinLogoTwo.style.fill = "#ffffff"
    buyCircleTwo.style.fill = "#ffffff"
    burgerLineOne.style.background = "#ffffff"
    burgerLineTwo.style.background = "#ffffff"
    burgerLineThree.style.background = "#ffffff"
  } else {
    urchinLogoOne.style.fill = "#000000"
    urchinLogoTwo.style.fill = "#000000"
    buyCircleTwo.style.fill = "#000000"
    burgerLineOne.style.background = "#000000"
    burgerLineTwo.style.background = "#000000"
    burgerLineThree.style.background = "#000000"
  }
})

// hamburgerBars.forEach(bar => {
//   const productFooter = urchinProduct.getBoundingClientRect().bottom

//   bar.addEventListener("scroll", e => {
//     if (productFooter < (window.innerHeight || document.documentElement.clientHeight)) {
//       e.style.background = "green"
//       console.log("bar bar bar")
//     } else {
//       e.style.background = "yellow"
//       console.log("no-bar no-bar no-bar")
//     }
//   })
// })
