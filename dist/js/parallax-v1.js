const urchinWrapper = document.querySelector(".urchin-wrapper")
const productOverlay = document.querySelector(".product-intro-overlay")
const catImage = document.querySelector(".product-intro-cat-img")
const mainBottle = document.querySelector(".product-intro-image-bottle")
const bottleScorchOne = document.querySelector(".scorch-parallax-bottle1")
const bottleScorchTwo = document.querySelector(".scorch-parallax-bottle2")
const bottleScorchThree = document.querySelector(".scorch-parallax-bottle3")
const bottleScorchFour = document.querySelector(".scorch-parallax-bottle4")
const bottleLuckyOne = document.querySelector(".lucky-parallax-bottle1")
const bottleLuckyTwo = document.querySelector(".lucky-parallax-bottle2")
const bottleLuckyThree = document.querySelector(".lucky-parallax-bottle3")
const bottleLuckyFour = document.querySelector(".lucky-parallax-bottle4")

if (mainBottle) {
  urchinWrapper.addEventListener("scroll", e => {
    const overlayTop = productOverlay.getBoundingClientRect().top
    let perc_scorch_X = (overlayTop * -1) / window.innerHeight + 1
    let perc_scorch_Y = (overlayTop * -1.7) / window.innerHeight + 1
    let perc_scorch_Y30 = (overlayTop * -3) / window.innerHeight + 1
    let perc_scorch_Y40 = (overlayTop * -4) / window.innerHeight + 1
    let perc_scorch_Y350 = (overlayTop * -35) / window.innerHeight + 1

    bottleScorchOne.style.transform = `translateX(${20 * perc_scorch_X}vw) translateY(${65 * perc_scorch_Y30}vh)`
    bottleScorchTwo.style.transform = `translateX(${-15 * perc_scorch_X}vw) translateY(${75 * perc_scorch_Y}vh)`
    bottleScorchThree.style.transform = `translateX(${15 * perc_scorch_X}vw) translateY(${5 * perc_scorch_Y350}vh)`
    bottleScorchFour.style.transform = `translateX(${-20 * perc_scorch_X}vw) translateY(${25 * perc_scorch_Y40}vh)`
  })
}

if (catImage) {
  urchinWrapper.addEventListener("scroll", e => {
    const overlayTop = productOverlay.getBoundingClientRect().top

    let perc_lucky_X30 = (overlayTop * -0.3) / window.innerHeight + 1
    let perc_lucky_X100 = (overlayTop * -1) / window.innerHeight + 1
    let perc_lucky_Y50 = (overlayTop * -0.5) / window.innerHeight + 1
    let perc_lucky_Y100 = (overlayTop * -1) / window.innerHeight + 1
    let perc_lucky_Y200 = (overlayTop * -3) / window.innerHeight + 1
    let perc_lucky_Y300 = (overlayTop * -3) / window.innerHeight + 1
    let perc_lucky_Y850 = (overlayTop * -8.5) / window.innerHeight + 1

    bottleLuckyOne.style.transform = `translateX(${5 * perc_lucky_X100}vw) translateY(${25 * perc_lucky_Y50}vh)`
    bottleLuckyTwo.style.transform = `translateX(${-30 * perc_lucky_X30}vw) translateY(${50 * perc_lucky_Y300}vh)`
    bottleLuckyThree.style.transform = `translateX(${28 * perc_lucky_X30}vw) translateY(${8 * perc_lucky_Y850}vh)`
    bottleLuckyFour.style.transform = `translateX(${-20 * perc_lucky_X100}vw) translateY(${10 * perc_lucky_Y200}vh)`
  })
}
