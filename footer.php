<?php

    echo '
    <style>
        .widget_nav_menu {
            list-style: none;
        }
    
        .widget_nav_menu ul {
                padding: 0;
            list-style: none;
        }
    </style>
        <section class="urchin-signoff">
            <div class="container d-flex pt-5 pt-md-3">
                <div class="row urchin-signoff-header mt-auto aos">
                    <h2 class="aos"><span class="aos" data-aos="fade-left" data-aos-delay="1000">dodge the</span><br /><span class="aos" data-aos="fade-right" data-aos-delay="1250">same old shots</span></h2>
                </div>

                <footer class="my-auto" style="width: 100%;">
                    <div class="row urchin-signoff-footer">
                    
                        <div class="col-xs-12 col-sm-6 col-lg-3 col-xl-2 row d-none d-md-block urchin-signoff-footer-section">
                            <img src="' . get_stylesheet_directory_uri() . '/dist/img/R7_Footer_Insta.svg" alt="" style="height: 2.25rem; text-align: left; justify-self: start; width: auto; margin-bottom: 0.5rem" />
                            <p class="urchin-signoff-footer-section-header">Instagram</p>
                            ';

                            dynamic_sidebar( 'footer_menu_1' );

                        echo '
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-lg-3 col-xl-2 row d-none d-md-block urchin-signoff-footer-section">
                            <img src="' . get_stylesheet_directory_uri() . '/dist/img//R7_Footer_Email.svg" alt="" class="d-none d-md-block" style="height: 2.25rem; text-align: left; justify-self: start; width: auto; margin-bottom: 0.5rem" />
                            <p class="urchin-signoff-footer-section-header">Contact Us</p>
                            ';

                            dynamic_sidebar( 'footer_menu_2' );

                        echo '
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-lg-3 col-xl-2 justify-content-center order-4 order-md-4 urchin-signoff-footer-section urchin-signoff-footer-section-nologo">
                            <p class="urchin-signoff-footer-section-header-terms mx-auto mx-md-0 d-none d-md-block">Terms &amp; Conditions</p>
                            ';
                            
                            dynamic_sidebar( 'footer_menu_3' );
                        
                        echo '
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3 col-xl-2 row d-block urchin-signoff-footer-section urchin-signoff-footer-section-nologo order-1 order-md-3">
                            <p class="urchin-signoff-footer-section-header mx-auto mx-md-0">Buy our spirits</p>
                            ';
                            
                            dynamic_sidebar( 'footer_menu_4' );
                        
                        echo '
                        </div>

                    </div>
                </footer>
            </div>
        </section>
    ' . 
                    
    // Close 'urchin-wrapper'
    
    '
    </div>
    ';

    wp_footer();

?>

<!-- Menu Column 3
    <a href="./terms.html" target="_blank" class="d-none d-md-block">Terms & Conditions</a>
    <div class="row d-md-none col-6 text-center">
        <a href="./terms.html" target="_blank" class="d-block d-md-none">Privacy</a>
    </div>
    <div class="row d-md-none col-6 text-center">
        <a href="./terms.html" target="_blank" class="d-block d-md-none">Terms</a>
    </div> -->