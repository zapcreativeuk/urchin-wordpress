<?php

function urch001_resources() {
    // Scripts
    wp_enqueue_script( 'urch001_dist-aos', '//unpkg.com/aos@2.3.1/dist/aos.js', array(), false, true  );
    wp_enqueue_script( 'urch001_dist-colours', get_template_directory_uri() . '/dist/js/icon-colours-v1.js', array(), false, true  );
    wp_enqueue_script( 'urch001_dist-parallax', get_template_directory_uri() . '/dist/js/parallax-v1.js', array(), false, true  );
    wp_enqueue_script( 'urch001_dist-main', get_template_directory_uri() . '/dist/js/main-v1.js', array(), false, true  );

    // Stylesheets
    wp_enqueue_style( 'urch001_aos_styles', '//unpkg.com/aos@2.3.1/dist/aos.css', NULL, microtime() );
    wp_enqueue_style( 'urch001_bootstrap_styles', get_template_directory_uri() . '/dist/css/bootstrap.css', NULL, microtime() );
    wp_enqueue_style( 'urch001_main_styles', get_template_directory_uri() . '/dist/css/main-v1.css', NULL, microtime() );
    wp_enqueue_style( 'urch001_styles', get_stylesheet_uri(), NULL, microtime() );
}

add_action( 'wp_enqueue_scripts', 'urch001_resources' );

function urch001_features() {
    add_theme_support( 'title-tag' );
    register_nav_menu( 'mainNav', 'Main Navigation' );
}

add_action( 'after_setup_theme', 'urch001_features' );

function urch001_widgets() {

    register_sidebar( array(
        'name'          => 'Index Sidebar',
        'id'            => 'index_sidebar',
        'before_title'  => '<p>',
        'after_title'   => '</p>',
        'before_widget' => '<div class="my-3">',
        'after_widget'  => '</div><hr>'
    ) );

    register_sidebar( array(
        'name'          => 'Footer Menu 1',
        'id'            => 'footer_menu_1',
        'before_title'  => '<p>',
        'after_title'   => '</p>'
    ) );

    register_sidebar( array(
        'name'          => 'Footer Menu 2',
        'id'            => 'footer_menu_2',
        'before_title'  => '<p>',
        'after_title'   => '</p>'
    ) );
    register_sidebar( array(
        'name'          => 'Footer Menu 3',
        'id'            => 'footer_menu_3',
        'before_title'  => '<p>',
        'after_title'   => '</p>'
    ) );

    register_sidebar( array(
        'name'          => 'Footer Menu 4',
        'id'            => 'footer_menu_4',
        'before_title'  => '<p>',
        'after_title'   => '</p>'
    ) );

}

add_action( 'widgets_init', 'urch001_widgets' );



/**
  * ACF Blocks
  */

    // Landing Block

    /**
     *  Check if ACF is activated
     */
    if ( function_exists( 'acf_register_block_type' ) ) {
        /**
         * Adding specific ACF action
         */
        add_action( 'acf/init', 'register_acf_block_types' );
    }

    function register_acf_block_types() {

    /**
     * Content Blocks
     */

        /**
         * Landing Block
         */
        acf_register_block_type(
            array(
                'name'              => 'landing',
                'title'             => __( 'Landing' ),
                'description'       => __( 'Urchin landing section' ),
                'render_template'   => 'template-parts/urchin-landing.php',
                'icon'              => 'airplane',
                'keywords'          => array(
                                        'landing',
                                        'opening section',
                                        'opening',
                                        'large text'
                                    )
                )
        ); 

        /**
         * portfolio Block
         */
        acf_register_block_type(
            array(
                'name'              => 'portfolio',
                'title'             => __( 'Portfolio' ),
                'description'       => __( 'Urchin portfolio section' ),
                'render_template'   => 'template-parts/urchin-portfolio.php',
                'icon'              => 'columns',
                'keywords'          => array(
                                        'portfolio',
                                        'opening section',
                                        'opening',
                                        'large text'
                                    )
                )
        ); 
    }
