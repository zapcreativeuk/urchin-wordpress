<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            
            <title>Urchin Spirits</title>
            <?php wp_head(); ?>

        </head>
        <body <?php body_class(); ?>>
        
            <?php

                echo '
                    <nav class="nav-sidebar">
                        <input type="checkbox" name="" id="check" class="checkbox" hidden>
                    
                        <div class="hamburger">
                            <label for="check" class="hamburger-menu">
                            <div class="hamburger-menu-line hamburger-menu-line-1"></div>
                            <div class="hamburger-menu-line hamburger-menu-line-2"></div>
                            <div class="hamburger-menu-line hamburger-menu-line-3"></div>
                            </label>
                        </div>

                        <a href="https://amazon.co.uk" target="_blank" class="navbar-buynow">
                            ' . urchinBuyNow() . '
                        </a>
                            
                    
                        <div class="nav-sidebar-navigation">
                            <ul class="nav-sidebar-list">
                                <li class="nav-sidebar-list-item">
                                    <a href="' . esc_url( home_url( '/' ) ) . '" class="nav-sidebar-list-item-link">Home</a>
                                </li>
                                <li class="nav-sidebar-list-item">
                                    <a href="' . esc_url( home_url( '/lucky' ) ) . '" class="nav-sidebar-list-item-link nav-sidebar-list-item-link-lucky">Lucky Sod</a>
                                </li>
                                <li class="nav-sidebar-list-item">
                                    <a href="' . esc_url( home_url( '/scorch' ) ) . '" class="nav-sidebar-list-item-link nav-sidebar-list-item-link-scorch">Scorch</a>
                                </li>
                            </ul>
                        </div>

                    </nav>

            
                    <header class="navbar fixed-top">
                        <div class="container">
                            <a class="navbar-brand" href="' . esc_url( home_url( '/' ) ) . '">
                                ' . urchinLogo( '9.875rem' ) . '
                            </a>
                        </div>
                    </header>
                    
                    <!-- Open: urchin-wrapper -->
                    <div class="urchin-wrapper">
                ';
            ?>
