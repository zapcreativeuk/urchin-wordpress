<?php

    get_header();

    /**
     * Global Variables
     */
    $lucky_page_title           = get_the_title();
    $portfolio_bg_lucky         = get_theme_file_uri() . '/dist/img/R7_Background_Lucky.svg';
    $portfolio_bg_lucky_black   = get_theme_file_uri() . '/dist/img/R7_Background_Lucky-black.svg';
    $lucky_flourish             = get_theme_file_uri() . '/dist/img/R7_Lucky_Slogan_Flourish.svg';
    
    /**
     * Page Variables
     */
        // Intro Section
        $intro_bg               = get_field( 'lucky_intro_background' );
        $intro_bottle           = get_field( 'lucky_intro_bottle' );
        $intro_parallax_bottle1 = get_field( 'lucky_intro_parallax_bottle_1' );
        $intro_parallax_bottle2 = get_field( 'lucky_intro_parallax_bottle_2' );
        $intro_parallax_bottle3 = get_field( 'lucky_intro_parallax_bottle_3' );
        $intro_parallax_bottle4 = get_field( 'lucky_intro_parallax_bottle_4' );
        $intro_title            = get_field( 'lucky_intro_title' );
        $intro_copy             = get_field( 'lucky_intro_copy' );
        $intro_m_logo           = get_field( 'lucky_intro_mobile_logo' );
        $intro_m_title          = get_field( 'lucky_mobile_title' );
        $intro_m_copy           = get_field( 'lucky_intro_mobile_copy' );
        $intro_down_arrow       = get_template_directory_uri() . '/dist/img/R7_Down_Arrow_Black.svg';

        // Suggestions Section
        $suggestions_title      = get_field( 'lucky_suggestions_title' );
        $suggestions_subtitle   = get_field( 'lucky_suggestions_subtitle' );
        $suggestions_footer     = get_field( 'lucky_suggestions_footer' );
        $suggestions_lgimg      = get_field( 'lucky_suggestions_lgimg' );
        $suggestions_mixers     = get_field( 'lucky_suggestions_mixers' );

        // Splash Section
        $splash_img             = get_field( 'lucky_splash_image' );
        $splash_slogan          = get_field( 'lucky_splash_slogan' );

        // CTA Section
        $cta_button             = get_field( 'lucky_cta_button' );
        $cta_link               = get_field( 'lucky_cta_link' );

    echo '
        <div class="product lucky">

            <section class="product-intro mb-5" style="background-image: none !important;">
    
                <div class="product-intro-overlay d-none d-md-block">
                    <img class="product-intro-overlay-parallax lucky-parallax-bottle1 d-none d-md-block" src="' . $intro_parallax_bottle1['url'] . '" alt="' . $intro_parallax_bottle1['alt'] . '" />
                    <img class="product-intro-overlay-parallax lucky-parallax-bottle2 d-none d-md-block" src="' . $intro_parallax_bottle2['url'] . '" alt="' . $intro_parallax_bottle2['alt'] . '" />
                    <img class="product-intro-overlay-parallax lucky-parallax-bottle3 d-none d-md-block" src="' . $intro_parallax_bottle3['url'] . '" alt="' . $intro_parallax_bottle3['alt'] . '" />
                    <img class="product-intro-overlay-parallax lucky-parallax-bottle4 d-none d-md-block" src="' . $intro_parallax_bottle4['url'] . '" alt="' . $intro_parallax_bottle4['alt'] . '" />
                </div>

                <div class="container d-flex flex-column">

                    <article class="product-intro-rabbit">
                        <h1 class="product-intro-cat-mobile-head mt-auto d-md-none" style="top: 5rem"
                            data-aos="fade-down"
                            data-aos-delay="1000"
                            data-aos-duration="1000"
                            data-aos-easing="ease-in-back"
                        >
                            LUCKY<br>SOD
                        </h1>

                        <div class="product-intro-cat-img mx-auto mt-auto aos"
                            data-aos="zoom-in"
                            data-aos-delay="500"
                            data-aos-duration="2000"
                            data-aos-easing="ease-in-back"
                        >
                            <img src="' . $intro_bg['url'] . '" alt="' . $intro_bg['alt'] . '" class="product-intro-cat-img-anim mt-auto aos" />
                        </div>
            
                        <p class="product-intro-cat-mobile-copy d-md-none"
                            data-aos="fade-up"
                            data-aos-delay="1500"
                            data-aos-duration="1000"
                            data-aos-easing="ease-in-back"
                        >
                            ' . $intro_m_copy . '
                        </p>
                        <img src="' . $intro_down_arrow . '" alt="" class="product-intro-mobile-arrow d-md-none" />
                    </article>

                    <article class="product-intro-text">
        
                        <div class="product-intro-text-wrapper aos" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500">
                        <div class="product-intro-text-title py-3 aos">
                            <h2 class="aos"
                                data-aos="fade-down"
                                data-aos-duration="1000"
                                data-aos-delay="500"
                            >' . $intro_title . '</h2>
                        </div>
                        <div class="product-intro-text-copy aos"
                            data-aos="fade-up"
                            data-aos-duration="1500"
                            data-aos-delay="1000">
                            ' . $intro_copy . '
                        </div>
                        </div>
            
                    </article>
                </div>

            </section>

            <section class="product-suggestions product-suggestions-cream d-none d-md-flex">
                <div class="product-suggestions-container d-flex flex-column justify-content-center align-items-center">
                    <h2 class="product-suggestions-title mt-auto aos" 
                    data-aos="fade-up" 
                    data-aos-delay="0" 
                    data-aos-duration="500" 
                    data-aos-easing="ease-in-back"
                    >' . $suggestions_title . '</h2>
                    <p class="product-suggestions-copy aos" 
                    data-aos="fade-up" 
                    data-aos-delay="0" 
                    data-aos-duration="750" 
                    data-aos-easing="ease-in-back" style="color: black !important; margin: 0; font-size: 2.249rem !important; line-height: 2.249rem !important;">' . $suggestions_subtitle . '</p>
                    <img class="product-suggestions-image my-3 aos" 
                    data-aos="zoom-in" 
                    data-aos-delay="1500" 
                    data-aos-duration="2000" 
                    data-aos-easing="ease-in-back" src="' . $suggestions_lgimg['url'] . '" alt="' . $suggestions_lgimg['alt'] . '" style="width: max(60vh, 40%);" />
                    <h3 class="product-suggestions-subtitle my-auto aos" 
                    data-aos="zoom-in" 
                    data-aos-delay="500" 
                    data-aos-duration="1000" data-aos-easing="ease-in-back" style="">' . $suggestions_footer . '</h3>
                </div>

            </section>

            <section class="product-suggestions" style="background: #3bbac9; padding: 5rem 0;">
                <div class="container">
                    <div class="row py-5 aos" data-aos="fade-up">';

                    if( have_rows('lucky_suggestions_mixers') ):
                        while( have_rows('lucky_suggestions_mixers') ) : the_row();
                    
                            $suggestion_name = get_sub_field('lucky_suggestions_mixer_name');
                            $suggestion_description = get_sub_field('lucky_suggestions_mixer_description');
                            $suggestion_description_long = get_sub_field('lucky_suggestions_mixer_description_long');
                            $suggestion_image = get_sub_field('lucky_suggestions_mixer_image');
                            echo '
                                <div class="col-12 col-md-4 product-suggestions-drink">
                                    <img class="product-suggestions-drink-img" src="' . $suggestion_image['url'] . '" alt="' . $suggestion_image['alt'] . '" />
                                    <div class="row product-suggestions-drink-wrapper">
                                    <div class="product-suggestions-drink-wrapper-title">
                                        <h4>' . $suggestion_name . '</h4>
                                    </div>
                                    <div class="product-suggestions-drink-wrapper-description">
                                        <span class="d-none d-md-block">' . $suggestion_description . '</span>
                                        <span class="d-block d-md-none">' . $suggestion_description_long . '</span>
                                    </div>
                                    </div>
                                </div>
                            ';
                    
                        endwhile;
                    endif;

                echo '
                    <div class="product-suggestions-dots">
                        <div class="product-suggestions-dots-dot active"></div>
                        <div class="product-suggestions-dots-dot"></div>
                        <div class="product-suggestions-dots-dot"></div>
                    </div>
                    </div>
                </div>
            </section>

            <section class="product-splash d-block" style="overflow: hidden;">
                <article class="product-slogan text-center d-md-none">
                    <div class="row">
                    <div class="product-slogan-text">
                        <img class="product-slogan-text-flourish aos" src="' . $lucky_flourish . '" alt=""
                            data-aos="flip-down"
                            data-aos-duration="750"
                            data-aos-delay="500"
                        >
                        <h3 class="aos" data-aos="fade-in">
                            ' . $splash_slogan . '
                        </h3>
                        <img class="product-slogan-text-flourish aos" src="' . $lucky_flourish . '" alt=""
                            data-aos="flip-up"
                            data-aos-duration="750"
                            data-aos-delay="500"
                        >
                    </div>
                    </div>
                </article>
                <img class="product-splash-img" src="' . $splash_img['url'] . '" alt="' . $splash_img['alt'] . '" />
            </section>

            <section class="product-buy d-md-none">
            <div class="container d-flex flex-column justify-content-evenly align-items-center">
                <img src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" class="product-buy-bottle mt-auto aos" data-aos="fade-down" />
                <a href="' . $cta_link . '" class="product-buy-button mt-5 mb-auto aos" data-aos="fade-up">' . ( $cta_button ? $cta_button : 'Buy ' . $lucky_page_title ) . '</a>
            </div>
            </section>
    
        </div>
    ';

    get_footer();

?>