<?php
    /**
     * Scorch Whisky Product Page Template
     */

    
    get_header();
    
    /**
     * Global Variables
     */
    $portfolio_bg_scorch        = get_theme_file_uri() . '/dist/img/R7_Background-Scorch.svg';
    $scorch_logo                = get_theme_file_uri() . '/dist/img/URCH001-logo-scorch.svg';
    
    /**
     * Page Variables
     */
        // Intro Section
        $intro_bg               = get_field( 'scorch_intro_background' );
        $intro_bottle           = get_field( 'scorch_intro_bottle' );
        $intro_parallax_bottle1 = get_field( 'lucky_intro_parallax_bottle_1' );
        $intro_parallax_bottle2 = get_field( 'lucky_intro_parallax_bottle_2' );
        $intro_parallax_bottle3 = get_field( 'lucky_intro_parallax_bottle_3' );
        $intro_parallax_bottle4 = get_field( 'lucky_intro_parallax_bottle_4' );
        $intro_title            = get_field( 'scorch_intro_title' );
        $intro_copy             = get_field( 'scorch_intro_copy' );
        $intro_m_logo           = get_field( 'scorch_intro_mobile_logo' );
        $intro_m_title          = get_field( 'scorch_mobile_title' );
        $intro_down_arrow       = get_template_directory_uri() . '/dist/img/R7_Down_Arrow_Black.svg';

        // Suggestions Section
        $suggestions_title      = get_field( 'scorch_suggestions_title' );
        $suggestions_subtitle   = get_field( 'scorch_suggestions_subtitle' );
        $suggestions_footer     = get_field( 'scorch_suggestions_footer' );
        $suggestions_lgimg      = get_field( 'scorch_suggestions_lgimg' );
        $suggestions_mixers     = get_field( 'scorch_suggestions_mixers' );

        // Splash Section
        $splash_img             = get_field( 'scorch_splash_img' );

        // CTA Section
        $cta_button             = get_field( 'scorch_cta_button' );
        $cta_link               = get_field( 'scorch_cta_link' );

     echo '
        <div class="product scorch">
            <section class="product-intro">
                <img class="product-intro-stag aos" src="' . $intro_bg['url'] . '" alt="' . $intro_bg['alt'] . '"
                    data-aos="zoom-in"
                    data-aos-delay="500"
                    data-aos-duration="3000"
                />
                <div class="product-intro-overlay d-none d-md-block aos">
                    <img data-delay="600" class="product-intro-overlay-parallax scorch-parallax-bottle1 d-none d-md-block aos" src="' . $intro_parallax_bottle1['url'] . '" alt="' . $intro_parallax_bottle1['alt'] . '" />
                    <img data-delay="600" class="product-intro-overlay-parallax scorch-parallax-bottle2 d-none d-md-block aos" src="' . $intro_parallax_bottle2['url'] . '" alt="' . $intro_parallax_bottle2['alt'] . '" />
                    <img data-delay="600" class="product-intro-overlay-parallax scorch-parallax-bottle3 d-none d-md-block aos" src="' . $intro_parallax_bottle3['url'] . '" alt="' . $intro_parallax_bottle3['alt'] . '" />
                    <img data-delay="600" class="product-intro-overlay-parallax scorch-parallax-bottle4 d-none d-md-block aos" src="' . $intro_parallax_bottle4['url'] . '" alt="' . $intro_parallax_bottle4['alt'] . '" />
                </div>

                <div class="container">
                    <article class="product-intro-image">
                        <img class="product-intro-image-bottle d-none d-md-block" src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" data-aos="fade-down" />
                        <img class="d-block d-md-none mt-auto" src="' . $scorch_logo . '" alt="" data-aos="zoom-in" data-aos-delay="1000" data-aos-duration="1500">
                        <p class="product-intro-mobile-subtitle d-md-none" data-aos="zoom-in" data-aos-duration="250" data-aos-delay="1000">' . $intro_m_title . '</p>
                        <div class="product-intro-image-arrow d-md-none mt-auto aos" data-aos="fade-up" data-aos-delay="1500">
                            <img src="' . $intro_down_arrow . '" alt="" class="product-intro-mobile-arrow" />
                        </div>
                    </article>

                    <article class="product-intro-text">
                
                        <div class="product-intro-text-wrapper aos" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500">
                            <div class="product-intro-text-title aos">
                                <h2 class="py-3 aos" style="" data-aos="fade-down" data-aos-duration="500" data-aos-delay="500">' . $intro_title . '</h2>
                            </div>
                            <div class="product-intro-text-copy aos" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1500">
                                ' . $intro_copy . '
                            </div>
                        </div>

                    </article>
                </div>


            </section>

            <section class="product-suggestions d-none d-md-flex flex-column justify-content-center align-items-center text-center aos">
                <div class="container">
                    <h2 class="product-suggestions-title mt-auto aos" 
                        data-aos="fade-down 
                        data-aos-delay="1000" 
                        data-aos-duration="500" 
                        data-aos-easing="ease-in-back"
                    >' . $suggestions_title . '</h2>

                    <p class="product-suggestions-copy aos" 
                        data-aos="fade-down" 
                        data-aos-delay="1000" 
                        data-aos-duration="750" style="">' . $suggestions_subtitle . '</p>
                    <img class="product-suggestions-image my-3 aos" 
                        data-aos="fade-up" 
                        data-aos-delay="2000" 
                        data-aos-duration="1000" 
                        data-aos-easing="ease-in-back" src="' . $suggestions_lgimg['url'] . '" alt="' . $suggestions_lgimg['alt'] . '" />
                    <h3 class="product-suggestions-subtitle my-auto aos" 
                        data-aos="fade-up" 
                        data-aos-delay="1500" 
                        data-aos-duration="1000" data-aos-easing="ease-in-back">' . $suggestions_footer . '</h3>
                </div>
            </section>

            <section class="product-suggestions">
                <div class="container">
                <div class="row py-5 aos" data-aos="fade-up">';

                if( have_rows('scorch_suggestions_mixers') ):
                    while( have_rows('scorch_suggestions_mixers') ) : the_row();
                
                        $suggestion_name = get_sub_field('scorch_suggestions_mixer_name');
                        $suggestion_description = get_sub_field('scorch_suggestions_mixer_description');
                        $suggestion_description_long = get_sub_field('scorch_suggestions_mixer_description_long');
                        $suggestion_image = get_sub_field('scorch_suggestions_mixer_image');
                        echo '
                            <div class="col-12 col-md-4 product-suggestions-drink">
                                <img class="product-suggestions-drink-img" src="' . $suggestion_image['url'] . '" alt="' . $suggestion_image['alt'] . '" />
                                <div class="row product-suggestions-drink-wrapper">
                                <div class="product-suggestions-drink-wrapper-title">
                                    <h4>' . $suggestion_name . '</h4>
                                </div>
                                <div class="product-suggestions-drink-wrapper-description">
                                    <span class="d-none d-md-block">' . $suggestion_description . '</span>
                                    <span class="d-block d-md-none">' . $suggestion_description_long . '</span>
                                </div>
                                </div>
                            </div>
                        ';
                
                    endwhile;
                    wp_reset_postdata();
                endif;

            echo '
                <div class="product-suggestions-dots aos" 
                    data-aos="zoom-in-up"
                    data-aos-delay="1000" 
                    data-aos-duration="1000"
                >
                    <div class="product-suggestions-dots-dot active"></div>
                    <div class="product-suggestions-dots-dot"></div>
                    <div class="product-suggestions-dots-dot"></div>
                    </div>
                </div>
                </div>
            </section>

            <section class="product-mobile-logo d-flex d-md-none">

                <div class="container">
            
                    <div class="product-mobile-logo-overlay">
                        <div class="product-mobile-logo-overlay-bottle product-mobile-logo-overlay-bottle1" >
                            <img class="product-mobile-logo-overlay-parallax aos" src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" data-aos="fade-right" data-aos-delay="650" />
                        </div>
                        <div class="product-mobile-logo-overlay-bottle product-mobile-logo-overlay-bottle2">
                            <img class="product-mobile-logo-overlay-parallax aos" src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" data-aos="fade-left" data-aos-delay="750" />
                        </div>
                        <div class="product-mobile-logo-overlay-bottle product-mobile-logo-overlay-bottle3">
                            <img class="product-mobile-logo-overlay-parallax aos" src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" data-aos="fade-right" data-aos-delay="850" />
                        </div>
                    </div>

                    <div class="row">
                        <img class="product-mobile-logo-scorch mt-auto aos" src="' . get_theme_file_uri() . '/dist/img/URCH001-logo-scorch.svg" data-aos="fade-right" />
                        <a href="https://amazon.co.uk" target="_blank" class="product-buy-button mt-auto">NOT SCOTCH</a>
                    </div>

                </div>
            

            </section>

            <section class="product-splash">

                <img class="product-splash-img" src="' . $splash_img['url'] . '" alt="' . $splash_img['alt'] . '" />
            
            </section>

            <section class="product-buy d-md-none">
                <div class="container d-flex flex-column justify-content-evenly align-items-center">
                    <img src="' . $intro_bottle['url'] . '" alt="' . $intro_bottle['alt'] . '" class="product-buy-bottle mt-auto aos" data-aos="fade-down" data-aos-delay="750" />
                    <a href="' . $cta_link . '" class="product-buy-button mt-5 mb-auto aos" data-aos="fade-up" data-aos-delay="1500">' . $cta_button . '</a>
                </div>
            </section>
        </div>
     ';

    get_footer();
?>
