<?php

    $urchin_landing_title       = get_field( 'landing_title' );
    $urchin_landing_subtitle    = get_field( 'landing_subtitle' );
    $urchin_landing_text        = get_field( 'landing_text' );

    echo '
        <section class="urchin-landing text-center py-lg-5">
            <div class="container">
                <div class="row urchin-landing-header my-auto">
                    <h1 class="aos" data-aos="fade-down" data-aos-delay="1500">
                        ' . $urchin_landing_title . ' <br class="d-sm-block d-lg-none" />
                        <span class="aos" data-aos="fade-up" data-aos-delay="2000">' . $urchin_landing_subtitle . '</span>
                    </h1>
                </div>
                <div class="row urchin-landing-subheader mb-5">
                    <p>' . $urchin_landing_text . '</p>
                    <a href="#urchin-portfolio">
                        <img class="d-lg-none" src="' . get_stylesheet_directory_uri() . '/dist/img/R7_Down_Arrow.svg" alt="" />
                    </a>
                </div>
            </div>
        </section>
  ';
?>
