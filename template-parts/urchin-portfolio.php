<?php

    $page                   = get_page_by_title( 'Urchin Spirits' );
    $page_id                = $page->ID;
    $portfolio_item         = get_field( 'portfolio_item' );

    echo '
        <section id="urchin-portfolio" class="urchin-portfolio">
    ';

    if( have_rows( 'portfolio_item' ) ):

        while( have_rows( 'portfolio_item' ) ) : the_row();

            $portfolio_name         = get_sub_field( 'portfolio_name' );
            $portfolio_background   = get_sub_field( 'portfolio_background_colour' );
            $portfolio_bg_overlay   = get_sub_field( 'portfolio_background_image' );
            $portfolio_button_img   = ( $portfolio_name === 'Scorch' ? scorchButton() : luckyButton() );
            $portfolio_bottle       = get_sub_field( 'portfolio_bottle_image' );
            $portfolio_link         = get_sub_field( 'portfolio_link' );


            echo '
                <div class="' . $portfolio_background . '">
                    <div class="urchin-portfolio-overlay">
                       <img src="' . $portfolio_bg_overlay['url'] . '" alt="' . $portfolio_bg_overlay['alt'] . '" class="aos"
                       data-aos="zoom-in"
                       data-aos-delay="100"
                       data-aos-duration="2000" />
                    </div>
                    <a class="urchin-portfolio-bottle-link" href="' . $portfolio_link['url'] . '" style="opacity: 1;">
                        <img src="' . $portfolio_bottle['url'] . '" alt="' . $portfolio_bottle['alt'] . '" class="urchin-portfolio-bottle aos" data-aos="fade-down" data-aos-duration="1250" data-aos-delay="1750" />
                    </a>
                    <a class="urchin-button urchin-button-scorch aos" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="2000" href="' . $portfolio_link['url'] . '">
                        ' . $portfolio_button_img . '
                    </a>
                </div>
                ';
            
            wp_reset_postdata();
                
            endwhile;
            
        endif;

    echo '
        </section>
    ';

?>




