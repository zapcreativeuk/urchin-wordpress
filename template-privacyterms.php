<?php
    /**
    * Template Name: Privacy & Terms Pages
    *
    * @package WordPress
    * @subpackage Urchin Spirits
    */

    get_header();

    $privacyTermsTitle      = get_the_title();
    $privacyTermsContent    = get_the_content();

    echo '
        <section class="terms">
            <div class="container">
                <div class="row terms-header justify-content-center">
                    <div class="col-12 col-md-8">
                        <h1>' . $privacyTermsTitle . '</h1>
                    </div>
                </div>
                <div class="row terms-content justify-content-center">
                    <div class="col-12 col-md-8">'
                    . $privacyTermsContent .
                '
                    </div>
                </div>
            </div>
        </section>
    ';

    get_footer();